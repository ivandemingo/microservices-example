<?php

namespace App\SharedKernel\Infrastructure\Bus\Query;

use App\SharedKernel\Domain\Bus\Query\Query;
use App\SharedKernel\Domain\Bus\Query\QueryBus;
use App\SharedKernel\Domain\Bus\Query\QueryHandler;
use App\SharedKernel\Domain\Bus\Query\Response;
use Exception;
use ReflectionClass;
use ReflectionNamedType;
use Symfony\Component\Messenger\Exception\HandlerFailedException;
use Symfony\Component\Messenger\Handler\HandlersLocator;
use Symfony\Component\Messenger\MessageBus;
use Symfony\Component\Messenger\Middleware\HandleMessageMiddleware;
use Symfony\Component\Messenger\Stamp\HandledStamp;

class SymfonyMessengerQueryBus implements QueryBus
{
    private MessageBus $bus;

    public function __construct(iterable $queryHandlers)
    {
        $this->bus = new MessageBus([
            new HandleMessageMiddleware($this->locatorForHandlers($queryHandlers)),
        ]);
    }

    public function ask(Query $query): ?Response
    {
        try {
            /** @var HandledStamp $stamp */
            $stamp = $this->bus->dispatch($query)->last(HandledStamp::class);
        } catch (HandlerFailedException $exception) {
            throw $exception->getPrevious();
        }

        return $stamp->getResult();
    }

    private function locatorForHandlers(iterable $queryHandlers): HandlersLocator
    {
        $location = [];

        foreach ($queryHandlers as $queryHandler) {
            $location[$this->getQueryClassName($queryHandler)] = [$queryHandler];
        }

        return new HandlersLocator($location);
    }

    private function getQueryClassName(QueryHandler $queryHandler): string
    {
        $reflector = new ReflectionClass($queryHandler);
        $method    = $reflector->getMethod('__invoke');

        /** @var ReflectionNamedType $fistParameterType */
        $fistParameterType = $method->getParameters()[0]->getType();

        if (null === $fistParameterType) {
            throw new Exception();
        }

        return $fistParameterType->getName();
    }
}
