<?php

namespace App\SharedKernel\Infrastructure\Bus\Command;

use App\SharedKernel\Domain\Bus\Command\Command;
use App\SharedKernel\Domain\Bus\Command\CommandBus;
use App\SharedKernel\Domain\Bus\Command\CommandHandler;
use Exception;
use ReflectionClass;
use ReflectionNamedType;
use Symfony\Component\Messenger\Handler\HandlersLocator;
use Symfony\Component\Messenger\MessageBus;
use Symfony\Component\Messenger\Middleware\HandleMessageMiddleware;

class SymfonyMessengerCommandBus implements CommandBus
{
    private MessageBus $bus;

    public function __construct(iterable $commandHandlers)
    {
        $this->bus = new MessageBus([
            new HandleMessageMiddleware($this->locatorForHandlers($commandHandlers)),
        ]);
    }

    public function dispatch(Command $command): void
    {
        $this->bus->dispatch($command);
    }

    private function locatorForHandlers(iterable $commandHandlers): HandlersLocator
    {
        $location = [];

        foreach ($commandHandlers as $commandHandler) {
            $location[$this->getCommandClassName($commandHandler)] = [$commandHandler];
        }

        return new HandlersLocator($location);
    }

    private function getCommandClassName(CommandHandler $commandHandler): string
    {
        $reflector = new ReflectionClass($commandHandler);
        $method    = $reflector->getMethod('__invoke');

        /** @var ReflectionNamedType $fistParameterType */
        $fistParameterType = $method->getParameters()[0]->getType();

        if (null === $fistParameterType) {
            throw new Exception();
        }

        return $fistParameterType->getName();
    }
}
