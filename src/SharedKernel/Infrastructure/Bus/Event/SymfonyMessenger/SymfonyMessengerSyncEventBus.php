<?php

namespace App\SharedKernel\Infrastructure\Bus\Event\SymfonyMessenger;

use App\SharedKernel\Domain\Bus\Event\DomainEvent;
use App\SharedKernel\Domain\Bus\Event\DomainEventSubscriber;
use App\SharedKernel\Domain\Bus\Event\EventBus;
use Symfony\Component\Messenger\Handler\HandlersLocator;
use Symfony\Component\Messenger\MessageBus;
use Symfony\Component\Messenger\Middleware\HandleMessageMiddleware;

class SymfonyMessengerSyncEventBus
{
    private MessageBus $bus;

    public function __construct(iterable $subscribers)
    {
        $this->bus = new MessageBus([
            new HandleMessageMiddleware($this->locatorForSubscribers($subscribers)),
        ]);
    }

    public function publish(DomainEvent ...$domainEvent): void
    {
        array_walk($domainEvent, $this->publisher());
    }

    private function publisher(): \Closure
    {
        return function (DomainEvent $domainEvent) {
            try {
                $this->bus->dispatch($domainEvent);
            } catch (\Throwable $throwable) {
                // TODO: Log exception
            }
        };
    }

    /** @param DomainEventSubscriber[] $subscribers */
    private function locatorForSubscribers(iterable $subscribers)
    {
        $location = [];

        foreach ($subscribers as $subscriber) {
            foreach ($subscriber::subscribedTo() as $domainEventClassName) {
                if (!array_key_exists($domainEventClassName, $location)) {
                    $location[$domainEventClassName] = [];
                }

                $location[$domainEventClassName][] = $subscriber;
            }
        }

        return new HandlersLocator($location);
    }
}
