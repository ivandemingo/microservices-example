<?php

namespace App\SharedKernel\Infrastructure\Bus\Event\OldSoundRabbitMq;

use App\SharedKernel\Domain\Bus\Event\DomainEvent;
use App\SharedKernel\Domain\Bus\Event\EventBus;
use OldSound\RabbitMqBundle\RabbitMq\Producer;

class OldSoundRabbitMqAsyncEventBus implements EventBus
{
    public function __construct(private Producer $producer)
    {
    }

    public function publish(DomainEvent ...$domainEvent): void
    {
        array_walk($domainEvent, $this->publisher());
    }

    private function publisher(): callable
    {
        return fn(DomainEvent $domainEvent) => $this->producer->publish(
            json_encode($domainEvent->serialize()),
            $domainEvent->name(),
            [],
            ['redelivery_count' => 0]
        );
    }
}
