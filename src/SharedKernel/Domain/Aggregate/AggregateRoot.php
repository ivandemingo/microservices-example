<?php

namespace App\SharedKernel\Domain\Aggregate;

use App\SharedKernel\Domain\Bus\Event\DomainEvent;

abstract class AggregateRoot
{
    /** @var DomainEvent[] */
    private array $domainEvents = [];

    protected function record(DomainEvent $domainEvent)
    {
        $this->domainEvents[] = $domainEvent;
    }

    public function pullDomainEvents(): array
    {
        $domainEvents = $this->domainEvents;

        $this->domainEvents = [];

        return $domainEvents;
    }
}
