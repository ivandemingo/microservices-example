<?php

namespace App\SharedKernel\Domain\Uuid;

use Stringable;

class Uuid implements Stringable
{
    private string $value;

    public function __construct(?string $value = null)
    {
        if (null !== $value && !\Symfony\Component\Uid\Uuid::isValid($value)) {
            throw new \Exception('Invalid Uuid with value: ' . $value); // TODO: Custom exception
        }

        $this->value = $value ?? \Symfony\Component\Uid\Uuid::v6()->toRfc4122();
    }

    public static function create(?string $value = null): static
    {
        return new static($value);
    }

    public function value(): string
    {
        return $this->value;
    }

    public function __toString()
    {
        return $this->value();
    }
}
