<?php

namespace App\SharedKernel\Domain\Bus\Event;

use App\SharedKernel\Domain\Uuid\Uuid;
use DateTime;
use DateTimeInterface;

abstract class DomainEvent
{
    private string $id;
    private DateTime $occurredOn;

    public function __construct(
        private string $aggregateId,
        ?string $id = null,
        ?DateTime $occurredOn = null
    ) {
        $this->id = $id ?? Uuid::create()->value();
        $this->occurredOn = $occurredOn ?? new DateTime();
    }

    abstract public function name(): string;

    abstract public static function deserialize(array $data): self;

    abstract protected function toPrimitives(): array;

    public function id(): string
    {
        return $this->id;
    }

    public function occurredOn(): DateTime
    {
        return $this->occurredOn;
    }

    public function aggregateId(): string
    {
        return $this->aggregateId;
    }

    public function serialize(): array
    {
        return [
            'id' => $this->id(),
            'name' => $this->name(),
            'occurredOn' => $this->occurredOn()->format(DateTimeInterface::ATOM),
            'data' => $this->toPrimitives(),
            'aggregateId' => $this->aggregateId(),
        ];
    }
}
