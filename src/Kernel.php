<?php

namespace App;

use Symfony\Bundle\FrameworkBundle\Kernel\MicroKernelTrait;
use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Symfony\Component\HttpKernel\Kernel as BaseKernel;
use Symfony\Component\Routing\Loader\Configurator\RoutingConfigurator;

class Kernel extends BaseKernel
{
    use MicroKernelTrait;

    public function __construct(string $environment, bool $debug, private string $appName)
    {
        parent::__construct($environment, $debug);
    }

    protected function getContainerClass(): string
    {
        return ucfirst($this->appName).parent::getContainerClass();
    }

    public function getCacheDir(): string
    {
        return $this->getProjectDir().'/var/cache/'.$this->appName.'/'.$this->environment;
    }

    public function getLogDir(): string
    {
        return $this->getProjectDir().'/var/log/'.$this->appName;
    }

    private function getConfigDir(): string
    {
        return $this->getProjectDir().'/config/shared';
    }

    private function getAppConfigDir(): string
    {
        return $this->getProjectDir().'/config/'.$this->appName;
    }

    private function configureContainer(ContainerConfigurator $container, LoaderInterface $loader, ContainerBuilder $builder): void
    {
        $this->doConfigureContainer($container, $this->getConfigDir());
        $this->doConfigureContainer($container, $this->getAppConfigDir());
    }

    private function doConfigureContainer(ContainerConfigurator $container, string $configDir): void
    {
        $container->import($configDir . '/{packages}/*.yaml');
        $container->import($configDir . '/{packages}/' . $this->environment . '/*.yaml');

        if (is_file($configDir . '/services.yaml')) {
            $container->import($configDir . '/services.yaml');
            $container->import($configDir . '/{services}_' . $this->environment . '.yaml');
        } else {
            $container->import($configDir . '/{services}.php');
        }
    }

    private function configureRoutes(RoutingConfigurator $routes): void
    {
        $this->doConfigRoutes($routes, $this->getConfigDir());
        $this->doConfigRoutes($routes, $this->getAppConfigDir());
    }

    private function doConfigRoutes(RoutingConfigurator $routes, string $configDir): void
    {
        $routes->import($configDir . '/{routes}/' . $this->environment . '/*.yaml');
        $routes->import($configDir . '/{routes}/*.yaml');

        if (is_file($configDir . '/routes.yaml')) {
            $routes->import($configDir . '/routes.yaml');
        } else {
            $routes->import($configDir . '/{routes}.php');
        }
    }
}
