<?php

namespace App\Web\Events\Domain;

class Event
{
    public function __construct(
        private string $id,
        private float $priceFrom,
        private float $discountedPriceFrom,
        private string $title,
        private string $description
    ) {
    }

    public static function create(
        string $id,
        float $priceFrom,
        float $discountedPriceFrom,
        string $title,
        string $description
    ): self {
        return new self($id, $priceFrom, $discountedPriceFrom, $title, $description);
    }

    public function id(): string
    {
        return $this->id;
    }

    public function priceFrom(): float
    {
        return $this->priceFrom;
    }

    public function discountedPriceFrom(): float
    {
        return $this->discountedPriceFrom;
    }

    public function title(): string
    {
        return $this->title;
    }

    public function description(): string
    {
        return $this->description;
    }
}
