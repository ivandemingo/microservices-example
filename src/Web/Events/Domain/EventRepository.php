<?php

namespace App\Web\Events\Domain;

interface EventRepository
{
    public function save(Event $event): void;
}
