<?php

namespace App\Web\Events\Application\Save;

use App\Admin\Events\Domain\EventCreatedDomainEvent;
use App\SharedKernel\Domain\Bus\Event\DomainEventSubscriber;

class SaveEventOnAdminEventCreated implements DomainEventSubscriber
{
    public function __construct(private SaveEventService $service)
    {
    }

    public static function subscribedTo(): array
    {
        return [
            EventCreatedDomainEvent::class,
        ];
    }

    public function __invoke(EventCreatedDomainEvent $domainEvent): void
    {
        $this->service->__invoke(
            $domainEvent->aggregateId(),
            $domainEvent->title(),
            $domainEvent->description()
        );
    }
}
