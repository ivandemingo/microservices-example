<?php

namespace App\Web\Events\Application\Save;

use App\Web\Events\Domain\Event;
use App\Web\Events\Domain\EventRepository;

class SaveEventService
{
    public function __construct(private EventRepository $repository)
    {
    }

    public function __invoke(
        string $id,
        string $title,
        string $description
    ): void {
        // TODO: Get price from

        $event = new Event($id, 0, 0, $title, $description);

        $this->repository->save($event);
    }
}
