<?php

namespace App\Extranet\Events\Application\Get;

use App\Extranet\Events\Domain\Event;
use App\Extranet\Events\Domain\EventId;
use App\Extranet\Events\Domain\EventRepository;

class GetEventService
{
    public function __construct(private EventRepository $repository)
    {
    }

    public function __invoke(string $id): Event
    {
        $event = $this->repository->find(EventId::create($id));

        if (null === $event) {
            throw new \Exception('Events not found'); // TODO: Custom Exception
        }

        return $event;
    }
}
