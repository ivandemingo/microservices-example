<?php

namespace App\Extranet\Events\Application\Get;

use App\SharedKernel\Domain\Bus\Query\Query;

class GetEventQuery implements Query
{
    public function __construct(private string $id)
    {
    }

    public function id(): string
    {
        return $this->id;
    }
}
