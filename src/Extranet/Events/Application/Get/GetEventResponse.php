<?php

namespace App\Extranet\Events\Application\Get;

use App\SharedKernel\Domain\Bus\Query\Response;

class GetEventResponse implements Response
{
    public function __construct(
        private string $id,
        private int $status,
        private string $title,
        private string $description
    ) {
    }

    public function id(): string
    {
        return $this->id;
    }

    public function status(): int
    {
        return $this->status;
    }

    public function title(): string
    {
        return $this->title;
    }

    public function description(): string
    {
        return $this->description;
    }
}
