<?php

namespace App\Extranet\Events\Application\Get;

use App\SharedKernel\Domain\Bus\Query\QueryHandler;

class GetEventQueryHandler implements QueryHandler
{
    public function __construct(private GetEventService $service)
    {
    }

    public function __invoke(GetEventQuery $query): GetEventResponse
    {
        $event = $this->service->__invoke($query->id());

        return new GetEventResponse(
            $event->id()->value(),
            $event->status(),
            $event->title(),
            $event->description()
        );
    }
}
