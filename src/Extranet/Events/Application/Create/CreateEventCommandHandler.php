<?php

namespace App\Extranet\Events\Application\Create;

use App\SharedKernel\Domain\Bus\Command\CommandHandler;

class CreateEventCommandHandler implements CommandHandler
{
    public function __construct(private CreateEventService $service)
    {
    }

    public function __invoke(CreateEventCommand $command): void
    {
        $this->service->__invoke(
            $command->id(),
            $command->status(),
            $command->title(),
            $command->description()
        );
    }
}
