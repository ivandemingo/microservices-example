<?php

namespace App\Extranet\Events\Application\Create;

use App\SharedKernel\Domain\Bus\Command\Command;

class CreateEventCommand implements Command
{
    public function __construct(
        private string $id,
        private int $status,
        private string $title,
        private string $description
    ) {
    }

    public function id(): string
    {
        return $this->id;
    }

    public function status(): int
    {
        return $this->status;
    }

    public function title(): string
    {
        return $this->title;
    }

    public function description(): string
    {
        return $this->description;
    }
}
