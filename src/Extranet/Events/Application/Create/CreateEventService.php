<?php

namespace App\Extranet\Events\Application\Create;

use App\Extranet\Events\Domain\Event;
use App\Extranet\Events\Domain\EventRepository;
use App\SharedKernel\Domain\Bus\Event\EventBus;

class CreateEventService
{
    public function __construct(private EventRepository $repository, private EventBus $eventBus)
    {
    }

    public function __invoke(string $id, int $status, string $title, string $description): void
    {
        $event = Event::create($id, $status, $title, $description);

        $this->repository->save($event);

        $this->eventBus->publish(...$event->pullDomainEvents());
    }
}
