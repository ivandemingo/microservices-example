<?php

namespace App\Extranet\Events\Domain;

use App\SharedKernel\Domain\Aggregate\AggregateRoot;

class Event extends AggregateRoot
{
    private EventId $id;

    public function __construct(
        string $id,
        private int $status,
        private string $title,
        private string $description
    ) {
        $this->id = EventId::create($id);
    }

    public static function create(string $id, int $status, string $title, string $description): self
    {
        $event = new self($id, $status, $title, $description);

        $event->record(EventCreatedDomainEvent::create($event));

        return $event;
    }

    public function id(): EventId
    {
        return $this->id;
    }

    public function status(): int
    {
        return $this->status;
    }

    public function title(): string
    {
        return $this->title;
    }

    public function description(): string
    {
        return $this->description;
    }
}
