<?php

namespace App\Extranet\Events\Domain;

interface EventRepository
{
    public function find(EventId $id): ?Event;

    public function save(Event $event): void;
}
