<?php

namespace App\Extranet\Events\Domain;

use App\SharedKernel\Domain\Bus\Event\DomainEvent;
use DateTime;

class EventCreatedDomainEvent extends DomainEvent
{
    public function __construct(
        string $aggregateId,
        private int $status,
        private string $title,
        private string $description,
        ?string $id = null,
        ?DateTime $occurredOn = null
    ) {
        parent::__construct($aggregateId, $id, $occurredOn);
    }

    public static function create(Event $event): self
    {
        return new self($event->id()->value(), $event->status(), $event->title(), $event->description());
    }

    public function name(): string
    {
        return 'extranet.events.created';
    }

    public function status(): int
    {
        return $this->status;
    }

    public function title(): string
    {
        return $this->title;
    }

    public function description(): string
    {
        return $this->description;
    }

    protected function toPrimitives(): array
    {
        return [
            'status' => $this->status(),
            'title' => $this->title(),
            'description' => $this->description(),
        ];
    }

    public static function deserialize(array $data): self
    {
        return new self(
            $data['aggregateId'],
            $data['data']['status'],
            $data['data']['title'],
            $data['data']['description'],
            $data['id'],
            new DateTime($data['occurredOn'])
        );
    }
}
