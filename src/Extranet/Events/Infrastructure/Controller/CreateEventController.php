<?php

namespace App\Extranet\Events\Infrastructure\Controller;

use App\Extranet\Events\Application\Create\CreateEventCommand;
use App\SharedKernel\Domain\Bus\Command\CommandBus;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CreateEventController extends AbstractController
{
    public function __construct(private CommandBus $commandBus)
    {
    }

    #[Route('/extranet/api/events/{id}', methods: ['PUT'])]
    public function __invoke(string $id, Request $request): Response
    {
        $body = json_decode($request->getContent(), true);

        $this->commandBus->dispatch(new CreateEventCommand(
            $id,
            $body['status'],
            $body['title'],
            $body['description']
        ));

        return new Response('', Response::HTTP_NO_CONTENT);
    }
}
