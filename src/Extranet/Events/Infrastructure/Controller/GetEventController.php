<?php

namespace App\Extranet\Events\Infrastructure\Controller;

use App\Extranet\Events\Application\Get\GetEventQuery;
use App\Extranet\Events\Application\Get\GetEventResponse;
use App\SharedKernel\Domain\Bus\Query\QueryBus;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class GetEventController extends AbstractController
{
    public function __construct(private QueryBus $queryBus)
    {
    }

    #[Route('/extranet/api/events/{id}', methods: ['GET'])]
    public function __invoke(string $id): JsonResponse
    {
        /** @var GetEventResponse $response */
        $response = $this->queryBus->ask(new GetEventQuery($id));

        return new JsonResponse(
            [
                'id' => $response->id(),
                'status' => $response->status(),
                'title' => $response->title(),
                'description' => $response->description(),
            ]
        );
    }
}
