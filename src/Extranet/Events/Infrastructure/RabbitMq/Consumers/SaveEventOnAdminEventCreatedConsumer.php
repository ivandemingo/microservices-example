<?php

namespace App\Extranet\Events\Infrastructure\RabbitMq\Consumers;

use App\Admin\Events\Domain\EventCreatedDomainEvent;
use App\Extranet\Events\Application\Save\SaveEventOnAdminEventCreated;
use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;

class SaveEventOnAdminEventCreatedConsumer implements ConsumerInterface
{
    public function __construct(private SaveEventOnAdminEventCreated $subscriber)
    {
    }

    public function execute(AMQPMessage $msg)
    {
        $domainEvent = EventCreatedDomainEvent::deserialize(json_decode($msg->body, true));

        $this->subscriber->__invoke($domainEvent);
    }
}
