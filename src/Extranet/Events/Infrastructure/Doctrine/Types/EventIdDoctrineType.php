<?php

namespace App\Extranet\Events\Infrastructure\Doctrine\Types;

use App\Extranet\Events\Domain\EventId;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\StringType;

class EventIdDoctrineType extends StringType
{
    const EVENT_ID = 'event_id';

    public function getName(): string
    {
        return self::EVENT_ID;
    }

    /** @param EventId $value */
    public function convertToDatabaseValue($value, AbstractPlatform $platform): mixed
    {
        return $value->value();
    }

    /** @param string $value */
    public function convertToPHPValue($value, AbstractPlatform $platform): mixed
    {
        return EventId::create($value);
    }
}
