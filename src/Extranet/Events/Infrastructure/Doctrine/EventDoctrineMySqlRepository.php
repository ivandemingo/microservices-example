<?php

namespace App\Extranet\Events\Infrastructure\Doctrine;

use App\Extranet\Events\Domain\Event;
use App\Extranet\Events\Domain\EventId;
use App\Extranet\Events\Domain\EventRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping\ClassMetadata;

class EventDoctrineMySqlRepository implements EventRepository
{
    private EntityRepository $repository;

    public function __construct(private EntityManagerInterface $entityManager)
    {
        $this->repository = new EntityRepository($this->entityManager, new ClassMetadata(Event::class));
    }

    public function find(EventId $id): ?Event
    {
        return $this->repository->find($id);
    }

    public function save(Event $event): void
    {
        $this->entityManager->persist($event);
        $this->entityManager->flush(); // TODO: Handle transactions with a middleware in the bus
    }
}
