<?php

namespace App\Admin\Providers\Infrastructure\Persistence;

use App\Admin\Providers\Domain\Provider;
use App\Admin\Providers\Domain\ProviderRepository;

class ProviderFakeRepository implements ProviderRepository
{
    public function find(string $id): Provider
    {
        return new Provider(
            $id,
            'Provider Name',
            1
        );
    }

    public function save(Provider $provider): void
    {
        // do nothing
    }
}
