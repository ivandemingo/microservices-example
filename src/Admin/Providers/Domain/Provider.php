<?php

namespace App\Admin\Providers\Domain;

use App\SharedKernel\Domain\Aggregate\AggregateRoot;

class Provider extends AggregateRoot
{
    public function __construct(
        private string $id,
        private string $name,
        private int $totalEvents
    ) {
    }

    public function id(): string
    {
        return $this->id;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function totalEvents(): int
    {
        return $this->totalEvents;
    }

    public function incrementTotalEvents(): void
    {
        $this->totalEvents++;

        // TODO: Record domain event
    }
}
