<?php

namespace App\Admin\Providers\Domain;

interface ProviderRepository
{
    public function find(string $id): Provider;

    public function save(Provider $provider): void;
}
