<?php

namespace App\Admin\Providers\Application\Increment;

use App\Admin\Events\Domain\EventCreatedDomainEvent;
use App\SharedKernel\Domain\Bus\Event\DomainEventSubscriber;

class IncrementTotalEventsOnEventCreated implements DomainEventSubscriber
{
    public function __construct(private IncrementTotalEventsService $service)
    {
    }

    public static function subscribedTo(): array
    {
        return [
            EventCreatedDomainEvent::class,
        ];
    }

    public function __invoke(EventCreatedDomainEvent $domainEvent): void
    {
        $this->service->__invoke($domainEvent->providerId());
    }
}
