<?php

namespace App\Admin\Providers\Application\Increment;

use App\Admin\Providers\Domain\ProviderRepository;
use App\SharedKernel\Domain\Bus\Event\EventBus;

class IncrementTotalEventsService
{
    public function __construct(private ProviderRepository $repository, private EventBus $eventBus)
    {
    }

    public function __invoke(string $providerId): void
    {
        $provider = $this->repository->find($providerId);

        if (null === $provider) {
            throw new \Exception('Provider with id: ' . $providerId . ' was not found');
            // TODO: Custom exception
        }

        $provider->incrementTotalEvents();

        $this->repository->save($provider);

        $this->eventBus->publish(...$provider->pullDomainEvents());
    }
}
