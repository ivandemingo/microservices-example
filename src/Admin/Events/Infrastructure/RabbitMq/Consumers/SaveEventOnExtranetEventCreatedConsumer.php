<?php

namespace App\Admin\Events\Infrastructure\RabbitMq\Consumers;

use App\Admin\Events\Application\Save\SaveEventOnExtranetEventCreated;
use App\Extranet\Events\Domain\EventCreatedDomainEvent;
use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;

class SaveEventOnExtranetEventCreatedConsumer implements ConsumerInterface
{
    public function __construct(private SaveEventOnExtranetEventCreated $subscriber)
    {
    }

    public function execute(AMQPMessage $msg)
    {
        $domainEvent = EventCreatedDomainEvent::deserialize(json_decode($msg->body, true));

        $this->subscriber->__invoke($domainEvent);
    }
}
