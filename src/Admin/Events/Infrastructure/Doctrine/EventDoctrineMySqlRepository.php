<?php

namespace App\Admin\Events\Infrastructure\Doctrine;

use App\Admin\Events\Domain\Event;
use App\Admin\Events\Domain\EventRepository;
use Doctrine\ORM\EntityManagerInterface;

class EventDoctrineMySqlRepository implements EventRepository
{
    public function __construct(private EntityManagerInterface $entityManager)
    {
    }

    public function save(Event $event): void
    {
        $this->entityManager->persist($event);
        $this->entityManager->flush(); // TODO: Handle transactions with a middleware in the bus
    }
}
