<?php

namespace App\Admin\Events\Infrastructure\Controller;

use App\Admin\Events\Application\Create\CreateEventCommand;
use App\SharedKernel\Domain\Bus\Command\CommandBus;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CreateEventController extends AbstractController
{
    public function __construct(private CommandBus $commandBus)
    {
    }

    #[Route('/admin/api/events/{id}', methods: ['PUT'])]
    public function __invoke(string $id, Request $request): Response
    {
        $body = json_decode($request->getContent(), true);

        $this->commandBus->dispatch(new CreateEventCommand(
            $id,
            $body['title'],
            $body['description'],
            $body['providerId'],
            $body['locationId'],
            $body['destinationId'],
            $body['status']
        ));

        return new Response('', Response::HTTP_NO_CONTENT);
    }
}
