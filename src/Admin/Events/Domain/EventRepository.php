<?php

namespace App\Admin\Events\Domain;

interface EventRepository
{
    public function save(Event $event): void;
}
