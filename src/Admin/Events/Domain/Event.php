<?php

namespace App\Admin\Events\Domain;

use App\SharedKernel\Domain\Aggregate\AggregateRoot;

class Event extends AggregateRoot
{
    public function __construct(
        private string $id,
        private string $title,
        private string $description,
        private string $providerId,
        private string $locationId,
        private string $destinationId,
        private int $status
    ) {
    }

    public static function create(
        string $id,
        string $title,
        string $description,
        string $providerId,
        string $locationId,
        string $destinationId,
        int $status
    ): self {
        $event = new self($id, $title, $description, $providerId, $locationId, $destinationId, $status);

        $event->record(EventCreatedDomainEvent::create($event));

        return $event;
    }

    public function id(): string
    {
        return $this->id;
    }

    public function title(): string
    {
        return $this->title;
    }

    public function description(): string
    {
        return $this->description;
    }

    public function providerId(): string
    {
        return $this->providerId;
    }

    public function locationId(): string
    {
        return $this->locationId;
    }

    public function destinationId(): string
    {
        return $this->destinationId;
    }

    public function status(): int
    {
        return $this->status;
    }
}
