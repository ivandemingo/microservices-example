<?php

namespace App\Admin\Events\Domain;

use App\SharedKernel\Domain\Bus\Event\DomainEvent;
use DateTime;

class EventCreatedDomainEvent extends DomainEvent
{
    public function __construct(
        string $aggregateId,
        private string $title,
        private string $description,
        private string $providerId,
        private string $locationId,
        private string $destinationId,
        private int $status,
        ?string $id = null,
        ?DateTime $occurredOn = null
    ) {
        parent::__construct($aggregateId, $id, $occurredOn);
    }

    public static function create(Event $event): self
    {
        return new self(
            $event->id(),
            $event->title(),
            $event->description(),
            $event->providerId(),
            $event->locationId(),
            $event->destinationId(),
            $event->status()
        );
    }

    public function name(): string
    {
        return 'admin.events.created';
    }

    public function title(): string
    {
        return $this->title;
    }

    public function description(): string
    {
        return $this->description;
    }

    public function providerId(): string
    {
        return $this->providerId;
    }

    public function locationId(): string
    {
        return $this->locationId;
    }

    public function destinationId(): string
    {
        return $this->destinationId;
    }

    public function status(): int
    {
        return $this->status;
    }

    public static function deserialize(array $data): self
    {
        return new self(
            $data['aggregateId'],
            $data['data']['title'],
            $data['data']['description'],
            $data['data']['providerId'],
            $data['data']['locationId'],
            $data['data']['destinationId'],
            $data['data']['status']
        );
    }

    protected function toPrimitives(): array
    {
        return [
            'title' => $this->title(),
            'description' => $this->description(),
            'providerId' => $this->providerId(),
            'locationId' => $this->locationId(),
            'destinationId' => $this->destinationId(),
            'status' => $this->status(),
        ];
    }
}
