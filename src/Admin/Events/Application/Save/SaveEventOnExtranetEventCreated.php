<?php

namespace App\Admin\Events\Application\Save;

use App\Extranet\Events\Domain\EventCreatedDomainEvent;
use App\SharedKernel\Domain\Bus\Event\DomainEventSubscriber;

class SaveEventOnExtranetEventCreated implements DomainEventSubscriber
{
    public function __construct(private SaveEventService $service)
    {
    }

    public static function subscribedTo(): array
    {
        return [
            EventCreatedDomainEvent::class,
        ];
    }

    public function __invoke(EventCreatedDomainEvent $domainEvent): void
    {
        // TODO: Handle status transformation between BCs and missing data

        $this->service->__invoke(
            $domainEvent->aggregateId(),
            $domainEvent->title(),
            $domainEvent->description(),
            '',
            '',
            '',
            $domainEvent->status()
        );
    }
}
