<?php

namespace App\Admin\Events\Application\Save;

use App\Admin\Events\Domain\Event;
use App\Admin\Events\Domain\EventRepository;
use App\SharedKernel\Domain\Bus\Event\EventBus;

class SaveEventService
{
    public function __construct(private EventRepository $repository, private EventBus $eventBus)
    {
    }

    public function __invoke(
        string $id,
        string $title,
        string $description,
        string $providerId,
        string $locationId,
        string $destinationId,
        int $status
    ) {
        $event = new Event(
            $id,
            $title,
            $description,
            $providerId,
            $locationId,
            $destinationId,
            $status
        );

        $this->repository->save($event);

        $this->eventBus->publish(...$event->pullDomainEvents());
    }
}
