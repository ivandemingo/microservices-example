<?php

namespace App\Admin\Events\Application\Create;

use App\Admin\Events\Domain\Event;
use App\Admin\Events\Domain\EventRepository;
use App\SharedKernel\Domain\Bus\Event\EventBus;

class CreateEventService
{
    public function __construct(private EventRepository $repository, private EventBus $eventBus)
    {
    }

    public function __invoke(
        string $id,
        string $title,
        string $description,
        string $providerId,
        string $locationId,
        string $destinationId,
        int $status
    ) {
        $event = Event::create(
            $id,
            $title,
            $description,
            $providerId,
            $locationId,
            $destinationId,
            $status
        );

        $this->repository->save($event);

        $this->eventBus->publish(...$event->pullDomainEvents());
    }
}
