<?php

namespace App\Admin\Events\Application\Create;

use App\SharedKernel\Domain\Bus\Command\CommandHandler;

class CreateEventCommandHandler implements CommandHandler
{
    public function __construct(private CreateEventService $service)
    {
    }

    public function __invoke(CreateEventCommand $command)
    {
        $this->service->__invoke(
            $command->id(),
            $command->title(),
            $command->description(),
            $command->providerId(),
            $command->locationId(),
            $command->destinationId(),
            $command->status()
        );
    }
}
