<?php

namespace App\Admin\Events\Application\Create;

use App\SharedKernel\Domain\Bus\Command\Command;

class CreateEventCommand implements Command
{
    public function __construct(
        private string $id,
        private string $title,
        private string $description,
        private string $providerId,
        private string $locationId,
        private string $destinationId,
        private int $status
    ) {
    }

    public function id(): string
    {
        return $this->id;
    }

    public function title(): string
    {
        return $this->title;
    }

    public function description(): string
    {
        return $this->description;
    }

    public function providerId(): string
    {
        return $this->providerId;
    }

    public function locationId(): string
    {
        return $this->locationId;
    }

    public function destinationId(): string
    {
        return $this->destinationId;
    }

    public function status(): int
    {
        return $this->status;
    }
}
