#!/bin/bash

# Nginx: environment variable substitution in configuration files
envsubst '${HOSTNAME}' < /etc/nginx/templates/default.conf.template > /etc/nginx/conf.d/default.conf

exec "$@"
