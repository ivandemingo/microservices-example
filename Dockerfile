FROM ubuntu:22.04

# Install PHP, NGINX, Supervisor and other libraries
ENV PHP_VERSION=8.1
ARG DEBIAN_FRONTEND=noninteractive
ENV TZ=Europe/Madrid
RUN apt-get update && apt-get install -y --no-install-recommends --no-install-suggests \
        gettext-base \
        git \
        iputils-ping \
        nginx \
        php${PHP_VERSION} \
        php${PHP_VERSION}-fpm \
        php${PHP_VERSION}-mbstring \
        php${PHP_VERSION}-mysql \
        php${PHP_VERSION}-sockets \
        php${PHP_VERSION}-xdebug \
        php${PHP_VERSION}-xml \
        php${PHP_VERSION}-zip \
        supervisor \
        unzip \
        zip && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# Install composer
COPY --from=composer:2 /usr/bin/composer /usr/local/bin/composer

# Copy root filesytem
COPY docker/rootfs /

# Copy consumers configuration
ARG APP_NAME
COPY config/$APP_NAME/supervisor/conf.d /etc/supervisor/conf.d

# Start commands
ENTRYPOINT ["sh", "/usr/bin/entrypoint.sh"]
CMD ["supervisord", "-c", "/etc/supervisor/supervisord.conf"]

WORKDIR /var/www
