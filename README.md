# Microservices example

## Stack

- PHP 8.1
- Symfony 5.4
- Composer 2
- MySQL 8.0
- NGINX
- Supervisor
- Docker
- Traefik
- RabbitMQ

![Stack diagram](docs/microservices-example.png)

## Architecture and design

- Domain-Driven Design (DDD)
- Command Query Responsibility Segregation (CQRS)
- Event-Driven Architecture

## Setup

- Initialize project automatically

```shell
make init
```

Once the init command is executed, you should be able to access the application in the following endpoints:

- ADMIN: `admin.localhost`
- EXTRANET: `extranet.localhost`
- WEB: `web.localhost`


- RabbitMQ: `rabbitmq.localhost`
- Traefik dashboard: `traefik.localhost`

More available commands:

- Build

```shell
make build
```

- Start

```shell
make start
```

- Configure environment

```shell
make configure
```

- Stop

```shell
make down
```

Execute `make help` for all available commands.
