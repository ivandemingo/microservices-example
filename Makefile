## ___  ____                                    _                 _____                          _
## |  \/  (_)                                  (_)               |  ___|                        | |
## | .  . |_  ___ _ __ ___  ___  ___ _ ____   ___  ___ ___  ___  | |____  ____ _ _ __ ___  _ __ | | ___
## | |\/| | |/ __| '__/ _ \/ __|/ _ \ '__\ \ / / |/ __/ _ \/ __| |  __\ \/ / _` | '_ ` _ \| '_ \| |/ _ \
## | |  | | | (__| | | (_) \__ \  __/ |   \ V /| | (_|  __/\__ \ | |___>  < (_| | | | | | | |_) | |  __/
## \_|  |_/_|\___|_|  \___/|___/\___|_|    \_/ |_|\___\___||___/ \____/_/\_\__,_|_| |_| |_| .__/|_|\___|
##                                                                                        | |
##                                                                                        |_|
##

dc-admin=docker-compose -p admin -f docker-compose.fpm.yml -f docker-compose.admin.yml
dc-extranet=docker-compose -p extranet -f docker-compose.fpm.yml -f docker-compose.extranet.yml
dc-web=docker-compose -p web -f docker-compose.fpm.yml -f docker-compose.web.yml
dc-shared=docker-compose -p shared -f docker-compose.yml

all: help

.PHONY: help
help:
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##//'

##Available common commands:
##

## init:					Initialize project automatically
.PHONY: init
init: build configure

## configure:				Configure environment for all services
.PHONY: configure
configure: start install migrate

## install:				Install dependencies for all services
.PHONY: install
install: install-admin install-extranet install-web

## start:					Start all services
.PHONY: start
start: start-shared start-admin start-extranet start-web

## down:					Stop all services
.PHONY: down
down: down-admin down-extranet down-web down-shared

## destroy:				Destroy all services (remove database data)
.PHONY: destroy
destroy: destroy-admin destroy-extranet destroy-web down-shared

## build:					Build all images
.PHONY: build
build: build-admin build-extranet build-web

## migrate:				Execute doctrine migrations for all services
.PHONY: migrate
migrate: migrate-admin migrate-extranet migrate-web

##
##Available SHARED commands:
##

## start-shared:				Start SHARED services
.PHONY: start-shared
start-shared:
	${dc-shared} up -d

## down-shared:				Stop SHARED services
.PHONY: down-shared
down-shared:
	${dc-shared} down --remove-orphans

## logs-shared:				Show logs for SHARED services
.PHONY: logs-shared
logs-shared:
	${dc-shared} logs -f

##
##Available ADMIN commands:
##

## install-admin:				Install dependencies for ADMIN services
.PHONY: install-admin
install-admin:
	${dc-admin} exec fpm composer install --no-interaction

## start-admin:				Start ADMIN services
.PHONY: start-admin
start-admin:
	${dc-admin} up -d

## start-admin:				Stop ADMIN services
.PHONY: start-admin
down-admin:
	${dc-admin} down --remove-orphans

## destroy-admin:				Destroy ADMIN services (remove database data)
.PHONY: destroy-admin
destroy-admin:
	${dc-admin} down --remove-orphans -v

## build-admin:				Build ADMIN images
.PHONY: build-admin
build-admin:
	${dc-admin} build

## run-admin:				Run a command in ADMIN FPM service
.PHONY: run-admin
run-admin:
	${dc-admin} exec fpm $(filter-out $@,$(MAKECMDGOALS))

## shell-admin:				Start a new shell in ADMIN FPM service
.PHONY: shell-admin
shell-admin:
	${dc-admin} exec fpm /bin/bash

## logs-admin:				Show logs for ADMIN FPM service
.PHONY: logs-admin
logs-admin:
	${dc-admin} logs -f fpm

## migrate-admin:				Execute doctrine migrations for ADMIN services
.PHONY: migrate-admin
migrate-admin:
	${dc-admin} exec fpm bin/console doctrine:migrations:migrate --no-interaction

##
##Available EXTRANET commands:
##

## install-extranet:			Install dependencies for EXTRANET services
.PHONY: install-extranet
install-extranet:
	${dc-extranet} exec fpm composer install --no-interaction

## start-extranet:			Start EXTRANET services
.PHONY: start-extranet
start-extranet:
	${dc-extranet} up -d

## down-extranet:				Stop EXTRANET services
.PHONY: down-extranet
down-extranet:
	${dc-extranet} down --remove-orphans

## destroy-extranet:			Destroy EXTRANET services (remove database data)
.PHONY: destroy-extranet
destroy-extranet:
	${dc-extranet} down --remove-orphans -v

## build-extranet:			Build EXTRANET images
.PHONY: build-extranet
build-extranet:
	${dc-extranet} build

## run-extranet:				Run a command in EXTRANET FPM service
.PHONY: run-extranet
run-extranet:
	${dc-extranet} exec fpm $(filter-out $@,$(MAKECMDGOALS))

## shell-extranet:			Start a new shell in EXTRANET FPM service
.PHONY: shell-extranet
shell-extranet:
	${dc-extranet} exec fpm /bin/bash

## logs-extranet:				Show logs for EXTRANET FPM service
.PHONY: logs-extranet
logs-extranet:
	${dc-extranet} logs -f fpm

## migrate-extranet:			Execute doctrine migrations for EXTRANET services
.PHONY: migrate-extranet
migrate-extranet:
	${dc-extranet} exec fpm bin/console doctrine:migrations:migrate --no-interaction

##
##Available WEB commands:
##

## install-web:				Install dependencies for WEB services
.PHONY: install-web
install-web:
	${dc-web} exec fpm composer install --no-interaction

## start-web:				Start WEB services
.PHONY: start-web
start-web:
	${dc-web} up -d

## down-web:				Stop WEB services
.PHONY: down-web
down-web:
	${dc-web} down --remove-orphans

## destroy-web:				Destroy WEB services (remove database data)
.PHONY: destroy-web
destroy-web:
	${dc-web} down --remove-orphans -v

## build-web:				Build WEB images
.PHONY: build-web
build-web:
	${dc-web} build

## run-web:				Run a command in WEB FPM service
.PHONY: run-web
run-web:
	${dc-web} exec fpm $(filter-out $@,$(MAKECMDGOALS))

## shell-web:				Start a new shell in WEB FPM service
.PHONY: shell-web
shell-web:
	${dc-web} exec fpm /bin/bash

## logs-web:				Show logs for WEB FPM service
.PHONY: logs-web
logs-web:
	${dc-web} logs -f fpm

## migrate-web:				Execute doctrine migrations for WEB services
.PHONY: migrate-web
migrate-web:
	${dc-web} exec fpm bin/console doctrine:migrations:migrate --no-interaction
